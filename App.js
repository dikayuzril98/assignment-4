import React, { Component } from 'react'
import { StyleSheet, AppState } from 'react-native'
import {StyleProvider } from 'native-base'
import getTheme from './native-base-theme/components'
import platform from './native-base-theme/variables/platform'
import {Provider} from 'react-redux'
import {Store} from './src/services/redux/Store'
import Routes from './Routes'
import firestore from '@react-native-firebase/firestore'
import auth from '@react-native-firebase/auth'
import messaging from '@react-native-firebase/messaging';

class App extends Component {
  state = {
    appState: AppState.currentState
  };
  componentDidMount() {
    AppState.addEventListener("change", this._handleAppStateChange);
  }
  componentWillUnmount() {
    AppState.removeEventListener("change", this._handleAppStateChange);
  }
  _handleAppStateChange = async nextAppState => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === "active") {
      this.updateOnline(true)
      console.log('App has come to the foreground!');
    }else{
      this.updateOnline(false)
      console.log('App On Background')
    }
    this.setState({ appState: nextAppState });
  }

  updateOnline = async (value) => {
    let myId = auth().currentUser.uid
    await firestore().collection('users').doc(myId).update({
      online: value
    })
  }

  render() {
    return (
      <Provider store={Store}>
          <StyleProvider style={getTheme(platform)}>
            <Routes />
          </StyleProvider>
      </Provider>
    )
  }
}
export default App
const styles = StyleSheet.create({})
