import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, ActivityIndicator } from 'react-native'
import {connect} from 'react-redux'
import {UpdateDeviceAction} from '../services/redux/Actions/DeviceInfoAction'
import auth from '@react-native-firebase/auth';
import Break from '../Components/Breaks/Break';
import messaging from '@react-native-firebase/messaging';
import DeviceInfo from 'react-native-device-info';
import firestore from '@react-native-firebase/firestore';

class WelcomeScreen extends Component {
    async componentDidMount(){
        let device_token = await messaging().getToken()
        console.log('device_token:', device_token)
        let manufacture = await DeviceInfo.getManufacturer()
        console.log('manufacture:',manufacture)
        let battery_level = await DeviceInfo.getBatteryLevel()
        console.log('Battery Level:',battery_level)
        let device_name = await DeviceInfo.getDeviceName()
        console.log('Device Name:',device_name)
        let brand = DeviceInfo.getBrand();
        console.log('brand:',brand)
        let device_id = DeviceInfo.getDeviceId();
        console.log('deviceId:',device_id)
        let mac_address = await DeviceInfo.getMacAddress();
        console.log('deviceId:',mac_address)
        
        let data = {device_token, manufacture, battery_level, device_name, brand, device_id, mac_address}
        this.props.UpdateDeviceInfo(data)

        this.requestUserPermission()
        setTimeout(async () => {
            auth().onAuthStateChanged(user => {
                if(user){
                    // alert(JSON.stringify(user))
                    this.props.navigation.replace('Index')
                    this.updateOnline(true)
                }else{
                    this.props.navigation.replace('Signin')
                }
            });     
        },1000)
    }

    requestUserPermission = async () => {
        const authStatus = await messaging().requestPermission();
        const enabled = authStatus === messaging.AuthorizationStatus.AUTHORIZED ||authStatus === messaging.AuthorizationStatus.PROVISIONAL;
        if (enabled){
            console.log('Authorization status:', authStatus);
        }
    }

    updateOnline = async (value) => {
        let myId = auth().currentUser.uid
        await firestore().collection('users').doc(myId).update({
          online: value
        })
      }
    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../../assets/images/salt-logo.png')} />
                <Text style={styles.welcomeText}>Salt Academy Activity</Text>
                <Break />
                <ActivityIndicator color='blue' size={25} />
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        device: state.device
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        UpdateDeviceInfo : (data) => dispatch(UpdateDeviceAction(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeScreen)

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        justifyContent:'center',
        alignItems:'center'
    },
    welcomeText:{
        fontSize:17,
        fontWeight:'bold'
    }
})
