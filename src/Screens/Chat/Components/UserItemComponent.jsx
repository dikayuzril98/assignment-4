//import liraries
import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import {List, ListItem, Text, Left, Thumbnail, Body, Right, Badge } from 'native-base';

// create a component
const UserItemComponent = ({imageUrl, name, lastMessage, newMessage, createdAt, onPress, onLongPress}) => {
    let photo = imageUrl !== ''?imageUrl:'https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png'
    let date = new Date(createdAt);
    let hours = date.getHours();
    let minutes = "0" + date.getMinutes();
    let formattedTime = hours + ':' + minutes.substr(-2);

    return (
        <List>
            <ListItem avatar onPress={onPress} onLongPress={onLongPress}>
                <Left>
                    <Thumbnail source={{ uri: photo }} />
                </Left>
                <Body>
                    <View style={{flexDirection:'row'}}>
                        <Text style={newMessage?{fontWeight:'bold'}:{fontWeight:'normal'}}>{name}</Text>
                        {newMessage && (
                            <Badge style={styles.badgeStyle}></Badge>
                        )}
                    </View>
                    {lastMessage === '⊘ The last message has been deleted' && (
                        <Text note numberOfLines={1} style={newMessage?{fontWeight:'bold', fontStyle:'italic'}:{fontWeight:'normal', fontStyle:'italic'}}>{lastMessage}</Text>
                    )}
                    {lastMessage !== '⊘ The last message has been deleted' && (
                        <Text note numberOfLines={1} style={newMessage?{fontWeight:'bold'}:{fontWeight:'normal'}}>{lastMessage}</Text>
                    )}
                </Body>
                <Right>
                    <Text note style={newMessage?{fontWeight:'bold'}:{fontWeight:'normal'}}>{formattedTime}</Text>
                </Right>
            </ListItem>
        </List>
    );
};

UserItemComponent.defaultProps = {
    imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png',
    name: 'John Travolka',
    lastMessage: 'Doing what you like will always keep you happy . .',
    onPress: () => alert('On Press'),
    onLongPress: () => alert('On Long Press')
}

// define your styles
const styles = StyleSheet.create({
    badgeStyle: {
        height:12, 
        width:12, 
        backgroundColor:'orange',
        marginLeft:-7
    },
});

//make this component available to the app
export default UserItemComponent;
