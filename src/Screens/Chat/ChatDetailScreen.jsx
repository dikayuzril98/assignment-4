import React, { Component, Fragment } from 'react'
import { Text, StyleSheet, View, ImageBackground } from 'react-native'
import { connect } from 'react-redux'
import HeaderChatComponent from '../../Components/Headers/HeaderChatComponent'
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { Bubble, Day, GiftedChat, Send } from 'react-native-gifted-chat'
import { Container, Content, Icon, Thumbnail } from 'native-base'
import { GetChat, UpdateChat } from '../../services/redux/Actions/ChatAction'
import axios from 'axios'

class ChatDetailScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            online: false,
            messages: []
        }
        this.myId = auth().currentUser.uid
        this.myName = auth().currentUser.displayName
        this.userId = this.props.route.params.id
        this.targetPhoto = this.props.route.params.photoUrl
        this.targetName = this.props.route.params.title
        this.targetToken = this.props.route.params.device_token
    }


    async componentDidMount() {
        //Define on send message
        const targetUser = await firestore().collection('users').doc(this.userId)
        const thisUser = await firestore().collection('users').doc(this.myId)
        const getMessageFromThisUser = thisUser.collection('messages').doc(this.userId)
        const getMessageFromTargetUser = targetUser.collection('messages').doc(this.myId)

        //define for update newMessage history
        const historyThisUser = await firestore().collection('messages_history').doc(this.myId) 
        const getHistoryFromThisUser = historyThisUser.collection('history').doc(this.userId)

        //Update newMessage history from 
        getHistoryFromThisUser.update({
            newMessage: false
        })

        //Proccess Get Message Item
        getMessageFromThisUser.onSnapshot(documentSnapshot => {
            if (documentSnapshot.exists) {
                this.setState({
                    messages: documentSnapshot.data().messages
                });
            } else {
                getMessageFromTargetUser.onSnapshot(documentSnapshot2 => {
                    if (documentSnapshot2.exists) {
                        this.setState({
                            messages: documentSnapshot2.data().messages
                        });
                    } else {
                        this.setState({
                            messages: []
                        });
                    }
                })
            }
        })
        targetUser.onSnapshot(documentSnapshot => {
            this.setState({
                online: documentSnapshot.data().online
            })
        })
    }

    sendNotif = async (message, myPhoto) => {
        try {
            let data = {
                to: this.targetToken,
                notification:{
                    title: this.myName,
                    body: message,
                    mutable_content: true,
                    sound: 'Tri-tone',
                },
                data:{
                    fromId: this.myId,
                    fromName: this.myName,
                    photo: myPhoto
                }
             }
            const response = await axios.post('https://fcm.googleapis.com/fcm/send', data, {
                headers:{
                    'Content-Type': 'application/json',
                    'Authorization': 'key=AAAApxEJbkw:APA91bFkLwX8bxdxUZ2p0htQ5EpgenmqOqaJ-Phcm_qGQKhQtDymYXIu723Bt7QEZBU_KUJ-Jtdb5xGrug-Sxna71dxprOtDemCIGozhbk8gaMQ91RGK0OdVwgXr5IRvbFKx2fZQlCGX'
                }
            });
            console.log(response);
        }catch(error){
            console.error(error);
        }
    }

    onSend = async (messages) => {
        const thisUser = await firestore().collection('users').doc(this.myId)
        const targetUser = await firestore().collection('users').doc(this.userId)
        const getMessageFromThisUser = thisUser.collection('messages').doc(this.userId)
        const getMessageFromTargetUser = targetUser.collection('messages').doc(this.myId)

        //define on initial history message
        const historyTargetUser = await firestore().collection('messages_history').doc(this.userId)
        const historyThisUser = await firestore().collection('messages_history').doc(this.myId) 
        const getHistoryFromThisUser = historyThisUser.collection('history').doc(this.userId)
        const getHistoryFromTargetUser = historyTargetUser.collection('history').doc(this.myId)

        this.sendNotif(messages[0].text, (await thisUser.get()).data().image_url)
        // console.log((await thisUser.get()).data().image_url)

        //Operation For Manage History
        getHistoryFromThisUser.get().then(doc => {
            if(doc.exists){
                getHistoryFromThisUser.update({
                    createdAt: Date.now(),
                    last_message: messages[0].text,
                    newMessage: false
                })
                getHistoryFromTargetUser.get().then(doc => {
                    if(doc.exists){
                        getHistoryFromTargetUser.update({
                            createdAt: Date.now(),
                            last_message: messages[0].text,
                            newMessage: true
                        })
                    }else{
                        getHistoryFromTargetUser.set({
                            createdAt: Date.now(),
                            last_message: messages[0].text,
                            newMessage: true
                        })
                    }
                })
            }else{
                getHistoryFromThisUser.set({
                    createdAt: Date.now(),
                    last_message: messages[0].text,
                    newMessage: false
                })
                getHistoryFromTargetUser.get().then(doc => {
                    if(doc.exists){
                        getHistoryFromTargetUser.update({
                            createdAt: Date.now(),
                            last_message: messages[0].text,
                            newMessage: true
                        })
                    }else{
                        getHistoryFromTargetUser.set({
                            createdAt: Date.now(),
                            last_message: messages[0].text,
                            newMessage: true
                        })
                    }
                })
            }
        })

        //Send Message Operation 
        if(this.state.messages.length == 0) {
            getMessageFromThisUser.set({
                messages: firestore.FieldValue.arrayUnion({
                    _id: messages[0]._id,
                    text: messages[0].text,
                    createdAt: Date.now(),
                    user: {_id: this.myId}
                })
            })
        } else {
            getMessageFromThisUser.get().then(doc => {
                if (doc.exists) {
                    getMessageFromThisUser.update({
                        messages: firestore.FieldValue.arrayUnion({
                            _id: messages[0]._id,
                            text: messages[0].text,
                            createdAt: Date.now(),
                            user: { _id: this.myId }
                        })
                    })
                } else {
                    getMessageFromTargetUser.update({
                        messages: firestore.FieldValue.arrayUnion({
                            _id: messages[0]._id,
                            text: messages[0].text,
                            createdAt: Date.now(),
                            user: { _id: this.myId }
                        })
                    })
                }
            })
        }
        this.setState((previousState) => {
            return {
                messages: GiftedChat.append(previousState.messages, messages),
            }
        });
    }

    onDelete = async (messageIdToDelete) => {
        const dataId = this.state.messages.filter(message => message._id == messageIdToDelete)
        let lastItemIndex = this.state.messages.length
        let dataIndex = this.state.messages.indexOf(dataId[0])+lastItemIndex
        let updateHistory = lastItemIndex == dataIndex ? 
                            {createdAt: Date.now(), last_message: '⊘ The last message has been deleted'} : 
                            {createdAt: Date.now()} 

        //Define on send message
        const thisUser = await firestore().collection('users').doc(this.myId)
        const targetUser = await firestore().collection('users').doc(this.userId)
        const getMessageFromThisUser = thisUser.collection('messages').doc(this.userId)
        const getMessageFromTargetUser = targetUser.collection('messages').doc(this.myId)

        //define on initial history message
        const historyTargetUser = await firestore().collection('messages_history').doc(this.userId)
        const historyThisUser = await firestore().collection('messages_history').doc(this.myId) 
        const getHistoryFromThisUser = historyThisUser.collection('history').doc(this.userId)
        const getHistoryFromTargetUser = historyTargetUser.collection('history').doc(this.myId)

        //Delete From Database
        getMessageFromThisUser.get().then(doc => {
            if(doc.exists){
                getMessageFromThisUser.update({
                    messages: firestore.FieldValue.arrayRemove(dataId[0])
                })
                getHistoryFromThisUser.update(updateHistory)
                getHistoryFromTargetUser.update(updateHistory)
            }else{
                getMessageFromTargetUser.update({
                    messages: firestore.FieldValue.arrayRemove(dataId[0])
                })
                getHistoryFromThisUser.update(updateHistory)
                getHistoryFromTargetUser.update(updateHistory)
            }
        })


        this.setState(previousState => ({ 
            messages: previousState.messages.filter(message => message._id !== messageIdToDelete) 
        }))
    }
    
    onLongPress = (context, message) => {
        // console.log(message);
        const options = ['copy','Delete Message', 'Cancel'];
        const cancelButtonIndex = options.length - 1;
        context.actionSheet().showActionSheetWithOptions({
            options,
            cancelButtonIndex
        }, (buttonIndex) => {
            switch (buttonIndex) {
                case 0:
                    Clipboard.setString(message.text);
                    break;
                case 1:
                    this.onDelete(message._id) //pass the function here
                    break;
            }
        });
    }

    renderSend = (props) => (
        <Send {...props}>
            <View style={{ marginRight: 10, marginBottom: 5 }}>
                <Icon name='ios-send' />
            </View>
        </Send>
    )

    renderAvatar = () => {
        if (this.props.route.params.photoUrl !== '') {
            return <Thumbnail small source={{ uri: this.props.route.params.photoUrl }} style={{ marginVertical: 10 }} />
        } else {
            return <Thumbnail small source={{ uri: 'https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png' }} style={{ marginVertical: 10 }} />
        }
    }
    renderBubble = (props) => {
        return (
            <Bubble {...props} wrapperStyle={{
                left: {
                    backgroundColor: 'white',
                },
                right: {
                    backgroundColor: 'blue'
                }
            }} />
        )
    }
    renderDay(props) {
        return <Day {...props} textStyle={{color: 'black'}}/> 
    }
    render() {
        const { navigation } = this.props
        const { photoUrl, title, isOnline } = this.props.route.params

        this.state.messages.sort((a, b) => b.createdAt - a.createdAt)
        return (
            <Container>
                <HeaderChatComponent onBack={() => navigation.goBack()} title={title} photoUrl={photoUrl} isOnline={this.state.online} />
                {/* <Text>{JSON.stringify(this.state.messages)}</Text> */}
                <ImageBackground source={require('../../../assets/images/chat-background.jpg')} style={styles.image}>
                    <GiftedChat
                        renderSend={this.renderSend}
                        messages={this.state.messages}
                        onSend={messages => this.onSend(messages)}
                        user={{
                            _id: this.myId, name: 'The Name'
                        }}
                        renderAvatar={this.renderAvatar}
                        renderBubble={this.renderBubble}
                        renderDay={this.renderDay}
                        onLongPress={this.onLongPress}
                    />
                </ImageBackground>

            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        chat: state.chat
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        UpdateChat: (data) => dispatch(UpdateChat(data)),
        GetChat: () => dispatch(GetChat())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatDetailScreen)

const styles = StyleSheet.create({
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
})
