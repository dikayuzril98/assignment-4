import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, Dimensions, TouchableOpacity, Alert, KeyboardAvoidingView } from 'react-native'
import InputInline from '../../Components/Inputs/InputInline';
import Break from '../../Components/Breaks/Break';
import ButtonRounded from '../../Components/Buttons/ButtonRounded';
import { regEmail } from '../../services/Utility/Helpers';
import {connect} from 'react-redux'
import {SigninAction} from '../../services/redux/Actions/SigninAction'

const {width, height} = Dimensions.get('screen')
class SigninScreen extends Component {
    constructor(){
        super()
        this.state = {
            email: '',
            password: ''
        }
    }

    handleChangeText = inputType => value => {
        this.setState({
            [inputType] : value.replace(/\s/g, '')
        })
    }

    render() {
        return (
            <KeyboardAvoidingView style={styles.container}>
                <View style={styles.formContainer}>
                    <View style={styles.logoSection}>
                        <Image source={require('../../../assets/images/salt-logo.png')} />
                    </View>
                    {/* <Text style={{fontWeight: 'bold'}}>Salt Academy Activity</Text> */}
                    <Text style={{fontWeight: 'bold'}}>Assignment 4</Text>
                    
                    {/* ------------------FormInput----------------------------- */}
                    <View style={styles.inputContainer}>
                        <Text style={{fontWeight: 'bold', alignSelf:'center'}}>Please login with a registered account</Text>
                        <Break />
                        <InputInline placeholder='Username / Email' iconName='user' onChangeText={this.handleChangeText('email')} autoCapitalize='none' keyboardType='email-address' />
                        <Break />
                        <InputInline placeholder='password' iconName='key' onChangeText={this.handleChangeText('password')} password />
                        <Break />
                    </View>
                    {/* ------------------FormInput----------------------------- */}
                    <Break />
                    <ButtonRounded textColor='white' width={width/1.1} onPress={() => this.props.SigninAction(this.props.device.device_token, this.state.email, this.state.password)} name='Signin' loading={this.props.loadingState.isLoading} />
                    <Break />
                </View>
                {/* --------------Footer Text---------------------- */}
                <View style={{marginHorizontal:35}}>
                    <View style={{flexDirection:'row'}}>
                        <Text>Forgot Password ?</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ResetPassword')}>
                            <Text style={{color:'blue', marginHorizontal:5}}>Reset Password</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection:'row', marginVertical:5}}>
                        <Text>Don't have an account ?</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Signup')}>
                            <Text style={{fontWeight:'bold', color:'blue', marginHorizontal:5}}>Sign up</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* --------------Footer Text---------------------- */}
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        loadingState: state.loading,
        device: state.device
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        SigninAction: (device_token, email, password) => dispatch(SigninAction(device_token, email, password))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SigninScreen)

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        justifyContent:'center'
    },
    formContainer:{
        alignItems:'center',
        margin:25
    },
    logoSection:{
        marginBottom:10
    },
    inputContainer:{
        marginTop:25
    },
})
