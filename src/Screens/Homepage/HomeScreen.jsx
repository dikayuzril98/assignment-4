import React, { Component } from 'react'
import {View, StyleSheet, TouchableOpacity, Alert } from 'react-native'
import {connect} from 'react-redux'
import { Container, Content, Text} from 'native-base';
import HeaderComponent from '../../Components/Headers/HeaderComponent'
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import messaging from '@react-native-firebase/messaging';

class HomeScreen extends Component {

    componentDidMount(){
        messaging().onMessage(remoteMessage => {
            Alert.alert(remoteMessage.notification.title, remoteMessage.notification.body);
            console.log('notif:', remoteMessage)
        });
        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
                'Notification caused app to open from background state:',
                remoteMessage.data,
            );
            this.props.navigation.navigate('ChatDetail',{
                id: remoteMessage.data.fromId,
                title: remoteMessage.data.fromName,
                photoUrl: remoteMessage.data.photo,
            });
        });
    }

    handleLogout = async () => {
        auth().signOut()
        let myId = auth().currentUser.uid
        await firestore().collection('users').doc(myId).update({
            online: false
        })
    }

    render() {
        const {email, displayName} = auth().currentUser;
        const {device_token, device_id, manufacture, battery_level, device_name, brand, mac_address} = this.props.device
        return (
            <Container>
                <HeaderComponent title='Salt Academy Activity' rightIcon='ios-notifications-sharp' noShadow noLeft titleStyle={{fontWeight:'bold'}} badgeCount={23} />
                <Content padder>
                    <View style={{flexDirection:'row'}}>
                        <Text>Welcome, </Text>
                        <Text style={{fontWeight:'bold'}}>{displayName}</Text>
                    </View>
                    <View style={{borderBottomColor: 'black', borderBottomWidth: 1}}/>
                    <Text numberOfLines={1}>Device Token : {device_token}</Text>
                    <Text numberOfLines={1}>Device ID : {device_id}</Text>
                    <Text numberOfLines={1}>Manufacture : {manufacture}</Text>
                    <Text numberOfLines={1}>Battery Level : {battery_level*100}%</Text>
                    <Text numberOfLines={1}>Device Name : {device_name}</Text>
                    <Text numberOfLines={1}>Device Brand : {brand}</Text>
                    <Text numberOfLines={1}>Mac Address : {mac_address}</Text>
                    {/* <Text>{JSON.stringify(this.props.device)}</Text> */}
                    <TouchableOpacity onPress={this.handleLogout}>
                        <Text style={{fontWeight:'bold'}}>Logout</Text>
                    </TouchableOpacity>
                </Content>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        device: state.device
    }
}

export default connect(mapStateToProps)(HomeScreen)

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})
