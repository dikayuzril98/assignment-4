//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, ActivityIndicator } from 'react-native';

const {width} = Dimensions.get('window')
// create a component
const ButtonRounded = (props) => {
    return (
        <View style={styles.buttonContainer}>
            <TouchableOpacity onPress={props.onPress} disabled={props.loading}>
                <View style={[styles.buttonSection, props.loading?{backgroundColor:'blue', width:30, height:30}:{backgroundColor:props.buttonColor, width:props.width}]}>
                    {!props.loading && <Text style={{fontWeight: 'bold', color:props.textColor}}>{props.name}</Text>}
                    {props.loading && <ActivityIndicator color='white' size={30} />}
                </View>
            </TouchableOpacity>
        </View>
    );
};

ButtonRounded.defaultProps = {
    buttonColor: 'blue',
    textColor:'black',
    width: width/1.2
};

//make this component available to the app
export default ButtonRounded;

// define your styles
const styles = StyleSheet.create({
    buttonSection:{
        height: 40,
        alignItems:'center',
        justifyContent: 'center',
        borderRadius:25,
        elevation:2
    },
});

