//import liraries
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Thumbnail, Text, Subtitle } from 'native-base';

// create a component
const HeaderChatComponent = ({onBack, photoUrl, title, isOnline}) => {
    let photo = photoUrl !== ''?photoUrl:'https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png'
    return (
        <Header>
            <Left style={{right:10}}>
                <Button transparent onPress={onBack}>
                    <Icon name='arrow-back' />
                </Button>
            </Left>
            <Body style={{flexDirection: 'row', alignItems: 'center', right:35, flex:1}}>
                <View>
                    <Thumbnail small source={{uri: photo}} style={{marginHorizontal:10}} />
                </View>
                <View>
                    <Title>{title}</Title>
                    {isOnline && (
                        <Subtitle style={{color:'green'}}>Online</Subtitle>
                    )}
                </View>
            </Body>
            <Right>
                <View style={{flexDirection: 'row', left:10}}>
                    <Button transparent style={{left:25}}>
                        <Icon name='ios-videocam' />
                    </Button>
                    <Button transparent style={{left:15}}>
                        <Icon name='call' />
                    </Button>
                    <Button transparent>
                        <Icon name='ellipsis-vertical-sharp' />
                    </Button>
                </View>
            </Right>
        </Header>
    );
};

HeaderChatComponent.defaultProps = {
    onBack: () => alert('On Back Press'),
    title: 'Chat Detail',
    photoUrl: 'https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png'
}

//make this component available to the app
export default HeaderChatComponent;

// define your styles
const styles = StyleSheet.create({
    badgeSection: {
        backgroundColor: 'red',
        borderRadius: 50,
        height: 15,
        width: 15,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        zIndex: 1,
        left: 29
    }
});

