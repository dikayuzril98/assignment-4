//import liraries
import React, {Fragment, useState} from 'react';
import { View, StyleSheet, TextInput, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

const {width} = Dimensions.get('window')
// create a component
const InputBlock = (props, style) => {
      
    const [hide, setHide] = useState(true)
    const [focus, setFocus] = useState(false)

    const color = focus?'darkblue':props.color
    
    return (
        <View style={styles.container}>
            {/* <Text>{iconName}</Text> */}
            <View style={[{...style}, styles.inputSection, {borderColor: color}]}>
                <View style={{backgroundColor:'transparent', marginHorizontal:10}}>
                    {!props.password && (<AntDesign name={props.iconName} size={props.iconSize} color={color} />)}
                    {props.password && (<MaterialCommunityIcons name='form-textbox-password' size={props.iconSize} color={color} />)}
                </View>
                {!props.password && (
                    <TextInput {...props} style={{flex:0.8}} onFocus={() => setFocus(true)} onBlur={() => setFocus(false)} />
                )}
                {props.password && (
                    <Fragment>
                        <TextInput {...props} style={{flex:1}} onFocus={() => setFocus(true)} onBlur={() => setFocus(false)} secureTextEntry={hide} autoCapitalize='none' />
                        {hide && (
                            <Icon name='eye-slash' style={{margin:5}} size={20} color={color} onPress={() => setHide(!hide)}/>
                        )}
                        {!hide && (
                            <Icon name='eye' style={{margin:10}} size={20} color={color} onPress={() => setHide(!hide)}/>
                        )}
                    </Fragment>
                )}
            </View>
        </View>
    );
};

InputBlock.defaultProps = {
    iconName: 'user',
    iconSize: 25,
    color: 'black'
};

//make this component available to the app
export default InputBlock;

// define your styles
const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inputSection:{
        backgroundColor:'transparent',
        flexDirection:'row',
        alignItems:'center',
        width:width/1.1,
        borderWidth:2,
        marginHorizontal:15,
        height:50
        
    },
});

