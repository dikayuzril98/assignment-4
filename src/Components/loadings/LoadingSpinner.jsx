//import liraries
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Spinner } from 'native-base';

// create a component
const LoadingSpinner = () => {
    return (
        <View style={styles.container}>
            <Spinner color='blue' size={30} />
        </View>
    );
};

//make this component available to the app
export default LoadingSpinner;

// define your styles
const styles = StyleSheet.create({
    container: {
        backgroundColor:'white', 
        height:35, 
        width:35, 
        elevation:8, 
        alignItems:'center', 
        justifyContent:'center', 
        borderRadius:30, 
        alignSelf:'center', 
        marginVertical:10
    },
});

