const initialState = {
    uid: '',
    lastSignInTime: '',
    creationTime: '',
    phoneNumber: '',
    displayName: '',
    email: '',
    emailVerified: true,
    photoURL: '',
}

const ProfileReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'INITIAL-USER':
            return {
                ...state,
                uid: action.payload.uid,
                lastSignInTime: action.payload.lastSignInTime,
                creationTime: action.payload.creationTime,
                phoneNumber: action.payload.phoneNumber,
                displayName: action.payload.displayName,
                email: action.payload.email,
                emailVerified: action.payload.emailVerified,
                photoURL: action.payload.photoURL,
            }
        case 'DETACH-USER':
            return {
                uid: '',
                lastSignInTime: '',
                creationTime: '',
                phoneNumber: '',
                displayName: '',
                email: '',
                emailVerified: true,
                photoURL: '',
            }
        default:
            return state
    }
}