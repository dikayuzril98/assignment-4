const initialState = {
    program: '',
    name: '',
    username: '',
    password: '',
    confirm_password: '',
}

const AuthReducer = (state=initialState, action) => {
    switch(action.type){
        case 'CHANGE-FIELD':
            return{
                ...state,
                [action.payload.inputType]: action.payload.value
            }
        default:
            return state
    }
}

export default AuthReducer