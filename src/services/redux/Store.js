import {createStore, applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk'
import reducer from './Reducers'
import { composeWithDevTools } from 'redux-devtools-extension';


const Store = createStore(reducer, composeWithDevTools(
    applyMiddleware(thunk),
    // other store enhancers if any
));

// const Store = createStore(reducer, applyMiddleware(thunk))

export {Store}