import React from 'react'
import Ionicons from 'react-native-vector-icons/Ionicons';
// import {Icon} from 'native-base';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import WelcomeScreen from './src/Screens/WelcomeScreen'
import SigninScreen from './src/Screens/Auth/SigninScreen'
import SignupScreen from './src/Screens/Auth/SignupScreen'
import HomeScreen from './src/Screens/Homepage/HomeScreen'
import ResetPasswordScreen from './src/Screens/Auth/ResetPasswordScreen'
import ChatScreen from './src/Screens/Chat/ChatScreen';
import ProfileScreen from './src/Screens/Profile/ProfileScreen';
import ChatDetailScreen from './src/Screens/Chat/ChatDetailScreen';
import NewChatScreen from './src/Screens/Chat/NewChatScreen';
import CarouselScreen from './src/Screens/Profile/CarouselScreen'

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

const TabNavigator = () => {
    return (
        <Tab.Navigator screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;
                size = focused ? 25 : 20;
        
                if (route.name === 'Home') {
                    iconName = focused ? 'ios-home' : 'ios-home';
                } else if (route.name === 'Chat') {
                    iconName = focused ? 'ios-chatbox' : 'ios-chatbox';
                } else if (route.name === 'Notification') {
                    iconName = focused ? 'ios-notifications-sharp' : 'ios-notifications-sharp';
                }else{
                    iconName = focused ? 'ios-person' : 'ios-person';
                }
                // You can return any component that you like here!
                return <Ionicons name={iconName} size={size} color={color} />;
            },
        })}
            tabBarOptions={{
                activeTintColor: 'black',
                inactiveTintColor: 'gray',
                labelStyle: {
                    fontSize: 12,
                    fontWeight:'bold'
                },
            }}>
            <Tab.Screen name='Home' component={HomeScreen} />
            <Tab.Screen name='Chat' component={ChatScreen} />
            <Tab.Screen name='Profile' component={ProfileScreen} />
        </Tab.Navigator>
    )
}


const StackNavigator = () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false, ...TransitionPresets.SlideFromRightIOS, }}>
            <Stack.Screen name='Welcome' component={WelcomeScreen} />
            <Stack.Screen name='Signin' component={SigninScreen} />
            <Stack.Screen name='Signup' component={SignupScreen} />
            <Stack.Screen name='ResetPassword' component={ResetPasswordScreen} />
            <Stack.Screen name='Index' component={TabNavigator} />
            <Stack.Screen name='ChatDetail' component={ChatDetailScreen} />
            <Stack.Screen name='NewChat' component={NewChatScreen} />
            <Stack.Screen name='Carousel' component={CarouselScreen} />
        </Stack.Navigator>
    )
}

const Routes = () => {
    return (
        <NavigationContainer>
            <StackNavigator />
        </NavigationContainer>
    )
}

export default Routes