/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
// import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from "react-native-push-notification";
import messaging from '@react-native-firebase/messaging';


// Register background handler
messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.log('Message handled in the background!', remoteMessage);
    // PushNotification.localNotification({
    //     group: 69753308,
    //     subText: 'test123',
    //     color: '#8000ff',
    //     groupSummary: true,
    //     title: 'test-title', 
    //     message: 'test-message',
    //     data: 'test-data',
    // })
});


AppRegistry.registerComponent(appName, () => App);
